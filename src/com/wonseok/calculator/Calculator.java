package com.wonseok.calculator;

import java.io.IOException;
import java.io.PrintWriter;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/calculator")
public class Calculator extends HttpServlet {
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getMethod().equals("GET")) {
			System.out.println("GET 메소드가 호출되었습니다.");
		}else if(request.getMethod().contentEquals("POST")) {
			System.out.println("POST 메소드가 호출되었습니다.");
		}else {
			System.out.println("GET도 POST도 아닙니다.");
		}
		super.service(request, response);
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String exp = "0";
		
		Cookie[] cookies = request.getCookies();
		if(cookies != null) {
			for(Cookie cookie : cookies) {
				if("expression".equals(cookie.getName())) {
					exp = cookie.getValue();
					break;
				}
			}
		}
		response.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print("<!DOCTYPE html>\r\n" + 
				"<html>\r\n" + 
				"    <head>\r\n" + 
				"        <title>calculator</title>\r\n" + 
				"        <meta chatset=\"utf-8\">\r\n" + 
				"        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.css\">\r\n" + 
				"        <style>\r\n" + 
				"            header {\r\n" + 
				"                width : 80%;\r\n" + 
				"                margin : 10px auto;\r\n" + 
				"                text-align: center;\r\n" + 
				"                padding : 10px 0;\r\n" + 
				"                border-bottom: 1px solid;\r\n" + 
				"            }\r\n" + 
				"            header h1 {\r\n" + 
				"                font-size: 2rem;\r\n" + 
				"                font-weight: bold;\r\n" + 
				"            }\r\n" + 
				"\r\n" + 
				"            .container {\r\n" + 
				"                font-size : 1.5rem;\r\n" + 
				"                font-weight: bold;\r\n" + 
				"            }\r\n" + 
				"            .container table{\r\n" + 
				"                margin : 10px auto;\r\n" + 
				"            }\r\n" + 
				"            .container table .display {\r\n" + 
				"                background-color: #e9e9e9;\r\n" + 
				"                text-align: right;\r\n" + 
				"                padding : 10px;\r\n" + 
				"\r\n" + 
				"            }\r\n" + 
				"            .container table td input{\r\n" + 
				"                width : 70px;\r\n" + 
				"                height : 70px;\r\n" + 
				"            }\r\n" + 
				"            .container table td input:hover {\r\n" + 
				"                background-color: yellow;\r\n" + 
				"            }\r\n" + 
				"\r\n" + 
				"        </style>\r\n" + 
				"    </head>\r\n" + 
				"    <body>\r\n" + 
				"        <header>\r\n" + 
				"            <h1>서버기반의 계산기 앱</h1>\r\n" + 
				"        </header>\r\n" + 
				"        <section class=\"container\">\r\n" + 
				"            <form action=\"/make_calculator/calculator\" method=\"post\">\r\n" + 
				"                <table>\r\n" + 
				"                    <tr>\r\n"
				);
			out.printf("                        <td class=\"display\" colspan=\"4\">%s</td>\r\n" ,exp);
			out.print(
				"                    </tr>\r\n" + 
				"                    <tr>\r\n" + 
				"                        <td><input type=\"submit\" name=\"operator\" value=\"CE\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"operator\" value=\"C\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"operator\" value=\"BS\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"operator\" value=\"/\"></td>\r\n" + 
				"                    </tr>\r\n" + 
				"                    <tr>\r\n" + 
				"                        <td><input type=\"submit\" name=\"number\" value=\"7\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"number\" value=\"8\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"number\" value=\"9\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"operator\" value=\"*\"></td>\r\n" + 
				"                    </tr>\r\n" + 
				"                    <tr>\r\n" + 
				"                        <td><input type=\"submit\" name=\"number\" value=\"4\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"number\" value=\"5\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"number\" value=\"6\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"operator\" value=\"+\"></td>\r\n" + 
				"                    </tr>\r\n" + 
				"                    <tr>\r\n" + 
				"                        <td><input type=\"submit\" name=\"number\" value=\"1\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"number\" value=\"2\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"number\" value=\"3\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"operator\" value=\"-\"></td>\r\n" + 
				"                    </tr>\r\n" + 
				"                    <tr>\r\n" + 
				"                        <td><button style=\"width : 70px; height : 70px;\">empty</button></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"number\" value=\"0\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"dot\" value=\".\"></td>\r\n" + 
				"                        <td><input type=\"submit\" name=\"operator\" value=\"=\"></td>\r\n" + 
				"                    </tr>\r\n" + 
				"                </table>\r\n" + 
				"            </form>\r\n" + 
				"        </section>\r\n" + 
				"    </body>\r\n" + 
				"</html>");
		
		
		
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String exp = "";//쿠키값의 디폴드값
		Cookie[] cookies = request.getCookies();
		if(cookies != null) {
			for(Cookie cookie : cookies) {
				if(cookie.getName().equals("expression")) {
					exp = cookie.getValue();//이전 쿠기의 값
				}
			}
		}
		
		String op = request.getParameter("operator");
		String num = request.getParameter("number");
		String dot = request.getParameter("dot");
		
		if(op != null) {
			if(op.contentEquals("=")) {
				ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
				try {
					exp = String.valueOf(engine.eval(exp));
				} catch (ScriptException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else if(op.equals("C")) {
				exp = "";
			}else {
				exp += op;
			}
		}
		
		if(num != null) {
			exp += num;
		}
		
		if(dot != null) {
			exp += dot;
		}
		
		//쿠키 넣기
		Cookie cookie = new Cookie("expression", exp);
		cookie.setPath("/make_calculator/calculator");
		if(op != null && op.equals("C")) {
			cookie.setMaxAge(0);
		}
		response.addCookie(cookie);
		//리디렉드 보내기
		response.sendRedirect("/make_calculator/calculator");
	}
}
